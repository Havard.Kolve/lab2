package INF101.lab2;

import INF101.lab2.pokemon.Pokemon;

import INF101.lab2.pokemon.IPokemon;

public class Main {

    public static IPokemon pokemon1;
    public static IPokemon pokemon2;
    public static void main(String[] args) {
        // Have two pokemon fight until one is defeated
        Main.pokemon1 = new Pokemon("Lucario");
        Main.pokemon2 = new Pokemon("Bulbasaur");

        System.out.println(pokemon1);
        System.out.println(pokemon2);
        System.out.println();

        while(pokemon1.isAlive() && pokemon2.isAlive()) {
            pokemon1.attack(pokemon2);
            pokemon2.attack(pokemon1);
        }
    }
}

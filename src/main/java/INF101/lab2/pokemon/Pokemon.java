package INF101.lab2.pokemon;

import java.lang.Math;
import java.util.Random;

public class Pokemon implements IPokemon {
    String name;
    int healthPoints;
    int maxHealthPoints;
    int strength;
    Random random;
    int damageInflicted;


    public Pokemon(String name) {
        this.random = new Random();
        this.healthPoints = (int) Math.abs(Math.round(100 + 10 * random.nextGaussian()));
        this.maxHealthPoints = this.healthPoints;
        this.strength = (int) Math.abs(Math.round(20 + 10 * random.nextGaussian()));
        this.name=name;
    }

    public String getName() {
        return this.name;
    }

    @Override
    public int getStrength() {
        return this.strength;
    }

    @Override
    public int getCurrentHP() {
        return this.healthPoints;
    }

    @Override
    public int getMaxHP() {
        return this.maxHealthPoints;
    }

    public boolean isAlive() {
        if(getCurrentHP()>0){
            return true;
        }
        else{
            return false;
        }
    }
    @Override
    public void attack(IPokemon target) {
        int damageInflicted = (int) (this.strength + this.strength / 2 * random.nextGaussian());
        System.out.println(this.getName() + " attacks " + target.getName() + "."); 
        target.damage(damageInflicted);
        if (target.getCurrentHP() == 0) {
            System.out.println(target.getName() + " is defeated by " + this.getName() + ".");
        }
    }

    @Override
    public void damage(int damageTaken) {
        if (damageTaken >= 0) {
            healthPoints = healthPoints - damageTaken; 
        }
        if (healthPoints < 0) {
            healthPoints = 0;
        }
        if (damageTaken < 0) {
            damageTaken = 0;
        }
        if (damageTaken > maxHealthPoints) {
            damageTaken = maxHealthPoints;
        }
        
        String output = getName() + " takes " + damageTaken + " damage and is left with " + String.valueOf(getCurrentHP()) + "/" + String.valueOf(maxHealthPoints) + " HP";
        System.out.println(output);
    }
    

    @Override
    public String toString() {
        return this.name+ " HP: (" + String.valueOf(this.healthPoints)+"/"+ String.valueOf(this.maxHealthPoints)+") STR: "+ String.valueOf(this.strength);
    }

}
